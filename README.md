# git-hooks

This repo contains git hooks which can be applied on projects or globally on your system.

Current hooks:

| name        | purpose                                                                                                                                                             |
|-------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| commit-msg  |Add ticket name prefix for commit messages. It works for branches named e.g. "feature/IJ-97-migration-keycloak" and will prepend "IJ-97" to the commit message.      |

## How to install
You can set these hooks globally (executed for every git repo) by doing the following.

* check out this repository
* run `chmod -R +x hooks` on the hooks directory in this repository
* run  `git config --global core.hooksPath pathToThisReposHooksDirectory` e.g. `git config --global core.hooksPath ~/projects/git-hooks/hooks`